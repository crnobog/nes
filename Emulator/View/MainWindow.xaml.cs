﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Emulator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static RoutedCommand ViewDebuggerCommand = new RoutedCommand( );
        public static RoutedCommand ViewROMInfoCommand = new RoutedCommand( );

        ROMInfoWindow ROMInfoWindow;
        DebuggerWindow DebuggerWindow;

        EmulatorViewModel ViewModel;

        public MainWindow()
        {
            InitializeComponent();

            ViewModel = Application.Current.Resources["EVM"] as EmulatorViewModel;

            Loaded += ( sender, args ) => { ShowDebuggerWindow( ); };
        }
        
        private void OpenROM_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.Filter = "NES ROMs (*.nes)|*.nes";
            dlg.CheckFileExists = true;
            dlg.CheckPathExists = true;
            if (dlg.ShowDialog() == true)
            {
                try
                {
                    ViewModel.LoadROM( dlg.SafeFileName, dlg.OpenFile( ) );
                }
                catch( Exception )
                {
                    MessageBox.Show( "Invalid ROM file", "Error", MessageBoxButton.OK, MessageBoxImage.Error );
                }
            }
        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void CanExecuteViewDebuggerCommand( object sender, CanExecuteRoutedEventArgs e )
        {
            e.CanExecute = true;
        }

        private void ExecutedViewDebuggerCommand( object sender, ExecutedRoutedEventArgs e )
        {
            ShowDebuggerWindow( );
            e.Handled = true;
        }

        private void ShowDebuggerWindow( )
        {
            if( DebuggerWindow == null )
            {
                DebuggerWindow = new DebuggerWindow( );
                DebuggerWindow.Owner = this;
                DebuggerWindow.Closed += ( del_sender, del_args ) => { DebuggerWindow = null; };
                DebuggerWindow.Show( );
            }
        }

        private void CanExecuteViewROMInfoCommand( object sender, CanExecuteRoutedEventArgs e )
        {
            e.CanExecute = true;
        }

        private void ExecutedViewROMInfoCommand( object sender, ExecutedRoutedEventArgs e )
        {
            ShowROMInfoWindow( );
            e.Handled = true;
        }

        private void ShowROMInfoWindow( )
        {
            if( ROMInfoWindow == null )
            {
                ROMInfoWindow = new ROMInfoWindow( );
                ROMInfoWindow.Owner = this;
                ROMInfoWindow.Closed += ( del_sender, del_args ) => { ROMInfoWindow = null; };
                ROMInfoWindow.Show( );
            }
        }
    }
}
