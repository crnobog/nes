﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Emulator
{
    class EmulatorViewModel : ObservableObject
    {
        public ROM ROM { get; private set; }
        public CPUViewModel CPU { get; private set; }

        public EmulatorViewModel( )
        {
            CPU = new CPUViewModel( );
        }

        public void LoadROM( string name, System.IO.Stream stream )
        {
            ROM = new ROM( name, stream );
            RaisePropertyChangedEvent( "ROM" );
        }
    }
}
