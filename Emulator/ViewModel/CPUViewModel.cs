﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Emulator
{
    class CPUViewModel
    {
        public short PC
        {
            get;
            set;
        }
        public byte X
        {
            get;
            set;
        }
        public byte Y
        {
            get;
            set;
        }
        public byte A
        {
            get;
            set;
        }
        public byte SP
        {
            get;
            set;
        }
        public byte Flags
        {
            get;
            set;
        }

        public bool Negative
        {
            get;
            set;
        }
        public bool Overflow
        {
            get;
            set;
        }
        public bool Break
        {
            get;
            set;
        }
        public bool Decimal
        {
            get;
            set;
        }
        public bool InterruptDisable
        {
            get;
            set;
        }
        public bool Zero
        {
            get;
            set;
        }
        public bool Carry
        {
            get;
            set;
        }
    }
}
