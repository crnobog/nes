﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Emulator
{
    public enum Mirroring
    {
        Horizontal,
        Vertical
    }

    public enum Format
    {
        PAL,
        NTSC
    }

    // Order matters, matches .NES format
    public enum MapperType
    {
        None = 0,
        NintendoMMC1,
        UNROMSwitch,
        CNROMSwitch,
        NintendoMMC3,
        NintendoMMC5,

        Unsupported
    }

    public class ROM
    {
        public string Name { get; private set; }

        public int NumROMBanks { get { return ROMBanks.Length; } }
        public int NumVROMBanks { get { return VROMBanks.Length; } }
        public bool HasTrainer { get { return Trainer != null; } }
        public bool HasSRAM { get; private set;}

        public Mirroring Mirroring { get; private set; }
        public MapperType MapperType { get; private set; }
        public Format Format { get; private set; }
        public byte[] Trainer { get; private set; }
        public bool FourScreenVRAM { get; private set; }
        public bool VSSystem { get; private set; }
        public byte NumRAMBanks { get; private set; }

        public byte[][] ROMBanks { get; private set; }
        public byte[][] VROMBanks { get; private set; }

        public ROM( string name, System.IO.Stream stream )
        {
            Name = name.EndsWith(".nes") ? name.Remove(name.Length - 4) : name;
            try
            {
                using( System.IO.BinaryReader r = new System.IO.BinaryReader( stream ) )
                {
                    // Header
                    byte[] header = r.ReadBytes( 4 );
                    if( header[0] != 'N'
                        || header[1] != 'E'
                        || header[2] != 'S'
                        || header[3] != 0x1A )
                    {
                        throw new ArgumentException( "Invalid ROM header" );
                    }

                    byte numROMBanks = r.ReadByte( );
                    byte numVROMBanks = r.ReadByte( );
                    byte flags1 = r.ReadByte( );
                    byte flags2 = r.ReadByte( );

                    // Low bit of flags1 => vertical or horizontal mirroring
                    Mirroring = (flags1 & 0x1) != 0 ? Mirroring.Vertical : Mirroring.Horizontal;
                    // Bit 1 of flags1 => battery-backed SRAM
                    HasSRAM = (flags1 & 0x2) != 0;
                    // Bit 2 of flags1 => trainer
                    bool expectTrainer = (flags1 & 0x4) != 0;
                    // Bit 3 of flags1 => four screen VRAM
                    FourScreenVRAM = (flags1 & 0x8) != 0;

                    // Bits 4-7 of flags1 => low bits of rom mapper type
                    // Bits 4-7 of flags2 => high bits of rom mapper type
                    byte mapperLo = (byte)(flags1 >> 4);
                    byte mapperHi = (byte)(flags2 & 0xFF00);

                    MapperType = (MapperType)(mapperHi | mapperLo);
                    switch( MapperType )
                    {
                        case MapperType.None:
                        case MapperType.NintendoMMC1:
                        case MapperType.CNROMSwitch:
                        case MapperType.UNROMSwitch:
                        case MapperType.NintendoMMC3:
                        case MapperType.NintendoMMC5:
                            break;
                        default: 
                            MapperType = MapperType.Unsupported; 
                            break;
                    }

                    NumRAMBanks = r.ReadByte( );
                    NumRAMBanks = NumRAMBanks == 0 && HasSRAM ? (byte)1 : NumRAMBanks;

                    Format = (r.ReadByte( ) & 0x1) != 0 ? Format.PAL : Format.NTSC;

                    // Reserved
                    r.ReadBytes( 6 );

                    if( expectTrainer )
                    {
                        Trainer = r.ReadBytes( 512 );
                    }

                    // ROM banks
                    ROMBanks = new byte[numROMBanks][];
                    for( int i = 0; i < ROMBanks.Length; ++i )
                    {
                        ROMBanks[i] = r.ReadBytes( 16 * 1024 );
                    }

                    // VROM banks
                    VROMBanks = new byte[numVROMBanks][];
                    for( int i = 0; i < VROMBanks.Length; ++i )
                    {
                        VROMBanks[i] = r.ReadBytes( 8 * 1024 );
                    }
                }
            }
            catch( System.IO.IOException )
            {
                throw new ArgumentException( "Invalid ROM file" );
            }
        }
    }
}
